FactoryGirl.define do
  factory :event do
    latitude 1.5
    longitude 1.5
    name "MyString"
    address "MyString"
    date "2017-07-01 03:52:39"
    start "2017-07-01 03:52:39"
    finish "2017-07-01 03:52:39"
  end
end
