Rails.application.routes.draw do
  resources :attendances
  resources :users
  resources :locations
  resources :subjects
  resources :groups
  get 'users/create'

  get 'users/update'

  get 'users/destroy'

  get 'users/show'

  get 'users/index'

  get 'attendances/create'

  get 'attendances/update'

  get 'attendances/destroyindex'

  get 'attendances/show'

  resources :events
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "events#index"
end
