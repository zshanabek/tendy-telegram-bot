json.extract! subject, :id, :name, :start, :finish, :created_at, :updated_at
json.url subject_url(subject, format: :json)
