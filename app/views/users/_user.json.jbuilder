json.extract! user, :id, :username, :group_id, :count, :created_at, :updated_at
json.url user_url(user, format: :json)
