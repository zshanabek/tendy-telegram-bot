class Subject < ApplicationRecord
    has_many :events, dependent: :destroy
end
