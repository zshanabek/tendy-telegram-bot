class Event < ApplicationRecord
    has_one :attendance
    belongs_to :subject
    belongs_to :group    
    belongs_to :location
end
