# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Group.delete_all
Group.create! id:1, name: "Wozniak"
Group.create! id:2, name: "Jobs"

Subject.delete_all
Subject.create! id:1, name: "Ruby On Rails"
Subject.create! id:2, name: "Machine Learning"

Location.delete_all
Location.create! id:1, name: "Урок райлс", latitude: 51.119054391593636, longitude: 71.46126866340637, radius:50
Location.create! id:2,name: "Урок маркетинг", latitude: 61.119054391593636, longitude: 51.46126866340637, radius:50

Event.delete_all
Event.create! id:1, name: "Gorilla", location_id: 1, group_id:1, subject_id: 1, date: Date.today
Event.create! id:2, name: "Phoenix", location_id: 2, group_id:2, subject_id: 2, date: Date.today

User.delete_all
User.create! id:1, username: "Zhunissali", count: 0, group_id: 1
User.create! id:2, username: "Margulan", count: 0, group_id: 2

Attendance.delete_all
Attendance.create! id:1, user_id: 1, user_start:Time.now, user_finish:Time.now + 3*60*60, event_id: 1
Attendance.create! id:2, user_id: 2, user_start:Time.now, user_finish:Time.now + 4*60*60, event_id: 2
