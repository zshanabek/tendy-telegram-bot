class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.integer :location_id      
      t.integer :group_id    
      t.integer :subject_id
      t.datetime :date        

      t.timestamps
    end
  end
end
