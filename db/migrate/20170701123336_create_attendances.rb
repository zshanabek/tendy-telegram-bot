class CreateAttendances < ActiveRecord::Migration[5.1]
  def change
    create_table :attendances do |t|
      t.integer :user_id
      t.time :user_start
      t.time :user_finish
      t.integer :event_id

      t.timestamps
    end
  end
end
